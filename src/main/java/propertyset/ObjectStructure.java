package propertyset;
/*
 * Copyright (C) 2003, 2007 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.NoSuchElementException;
/**
 * @author andreasm
 */
public final class ObjectStructure
{
	private final PropertySet propertyset;
	/**
	 * Constructor for ObjectStructure.
	 */
	public ObjectStructure(PropertySet propertyset)
	{
		super();
		//
		this.propertyset = propertyset;
	}
	/**
	 * Method traverseWith
	 *
	 * @param visitor that is traversed over the object structzre
	 * @throws VisitorException thrown by the visitor
	 */
	public void traverseWith(final Visitor visitor) throws VisitorException
	{
		var list = new LinkedList<Move>();
		//
		list.addFirst(new Enter(propertyset));
		//
		try
		{
			while (!list.isEmpty())
			{
				var move = list.removeFirst();
				var parent = move.propertyset;
				//
				if (move.isEnter())
				{
					int nchilds = parent.getChildCount();
					int action = visitor.enter(parent);
					//
					switch (action)
					{
						case Visitor.LEAF :
							visitor.leave(parent);
							break;
							//
						case Visitor.DEPTH_FIRST :
							list.addFirst(new Leave(parent));
							//
							for (int i = nchilds - 1; i >= 0; i--)
							{
								list.addFirst(new Enter(parent.getChild(i)));
							}
							break;
							//
						case Visitor.BREADTH_FIRST :
							for (int i = 0; i < nchilds; i++)
							{
								list.addLast(new Enter(parent.getChild(i)));
							}
							//
							list.addLast(new Leave(parent));
							break;
                        //
                        case Visitor.END :
                            return;
					}
				}
				else
				{
					visitor.leave(parent);
				}
			}
		}
		catch (NoSuchElementException e)
		{
            throw new VisitorException(e);
		}
	}
}
