/*
 * Copyright (C) 2004, 2007, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package propertyset;
/*
 * Created on 29.11.2004
 */
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
/**
 * @author Andreasm
 */
public class Attributes
{
    private final LinkedList<Attribute> attributes;
    /**
     * Constructor for Attributes
     */
    public Attributes()
    {
        super();
        //
        attributes = new LinkedList<>();
    }
    /**
     * Returns the Value for a Name
     *
     * @param name Name
     * @return Value
     */
    public String getAttribute(String name)
    {
        for (var attribute : attributes)
        {
            if (attribute.hasName(name)) return attribute.getValue();
        }
        //
        return null;
    }
    /**
     * Sets the Value for a Name
     *
     * @param namespace Namespace
     * @param name Name
     * @param value Value
     */
    public void setAttribute(String namespace, String name, String value)
    {
        var attribute = new Attribute();
        attribute.setNamespace(namespace);
        attribute.setName(name);
        attribute.setValue(value);
        attributes.addFirst(attribute);
    }
    /*
     * Returns all Names
     */
    public String[] getNames()
    {
        var nameset = new HashSet<String>();
        //
        for (var attribute : attributes)
        {
            var name = attribute.getName();
            nameset.add(name);
        }
        //
        var namearray = new String[nameset.size()];
        nameset.toArray(namearray);
        Arrays.sort(namearray);
        //
        return namearray;
    }
}