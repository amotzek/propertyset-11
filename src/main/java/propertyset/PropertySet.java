package propertyset;
/*
 * Copyright (C) 2003 - 2005 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
/**
 * @author andreasm
 */
@SuppressWarnings("unused")
public class PropertySet
{
    private String namespace;
    private String name;
    private String value;
    private Attributes attributes;
    private ArrayList<PropertySet> childs;
    /**
     * Constructor for Property Set
     */
    public PropertySet()
    {
        super();
    }
    /**
     * Sets the Namespace
     *
     * @param namespace Namespace
     */
    public final void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }
    /**
     * Sets the Name
     *
     * @param name Name
     */
    public final void setName(String name)
    {
        this.name = name;
    }
    /**
     * Sets the Value
     *
     * @param value Value
     */
    public final void setValue(String value)
    {
        this.value = value;
    }
    /**
     * Sets the Attributes
     *
     * @param attributes Attributes
     */
    public final void setAttributes(Attributes attributes)
    {
        this.attributes = attributes;
    }
    /**
     * Sets an Attribute
     *
     * @param name  Attribute Name
     * @param value Attribute Value
     */
    public final void setAttribute(String name, String value)
    {
        if (attributes == null) attributes = new Attributes();
        //
        attributes.setAttribute(namespace, name, value);
    }
    /**
     * Gets the Attributes
     *
     * @return Attributes
     */
    public final Attributes getAttributes()
    {
        return attributes;
    }
    /**
     * Gets an Attribute Value
     *
     * @param name Attribute Name
     * @return Attribute Value
     */
    public final String getAttribute(String name)
    {
        if (attributes == null) return null;
        //
        return attributes.getAttribute(name);
    }
    /**
     * Returns the Namespace
     *
     * @return Namespace
     */
    public final String getNamespace()
    {
        return namespace;
    }
    /**
     * Returns the Name
     *
     * @return Name
     */
    public final String getName()
    {
        return name;
    }
    /**
     * Returns the Value
     *
     * @return Value
     */
    public final String getValue()
    {
        return value;
    }
    /**
     * Checks if this Property Set has the given Namespace
     *
     * @param namespace Namespace
     * @return true, if this Property Set has the given Namespace
     */
    public final boolean hasNamespace(String namespace)
    {
        if (namespace == null) return this.namespace == null;
        //
        return namespace.equals(this.namespace);
    }
    /**
     * Checks if this Property Set has the given Name
     *
     * @param name Name
     * @return true, if this Property Set has the given Name
     */
    public final boolean hasName(String name)
    {
        return name.equals(this.name);
    }
    /**
     * Checks if this Property Set has a Child Property Set with the given Name
     *
     * @param name Name
     * @return true, if this Property Set has a Child Property Set with the given Name
     */
    public final boolean hasChild(String name)
    {
        return getChild(name) != null;
    }
    /**
     * Adds a Child Property Set
     *
     * @param child Child Property Set
     */
    public final void addChild(PropertySet child)
    {
        if (childs == null) childs = new ArrayList<>(10);
        //
        childs.add(child);
    }
    /**
     * Adds a Child Property Set
     *
     * @param cname  Child Name
     * @param cvalue Child Value
     */
    @SuppressWarnings("UnusedReturnValue")
    public final PropertySet addChild(String cname, String cvalue)
    {
        var child = new PropertySet();
        child.setName(cname);
        child.setValue(cvalue);
        addChild(child);
        //
        return child;
    }
    /**
     * Arranges the Childs such that the Names of the Childs
     * conform to the order of the Names given in the Array
     *
     * @param cnames Array of Child Names
     */
    public final void arrangeChilds(String[] cnames)
    {
        if (childs == null) return;
        //
        var cnameset = new HashSet<String>();
        var cnamelist = new LinkedList<String>();
        Collections.addAll(cnameset, cnames);
        int index = 0;
        //
        for (var child : childs)
        {
            var cname1 = child.getName();
            //
            if (cnamelist.contains(cname1)) continue;
            //
            if (cnameset.contains(cname1))
            {
                while (index < cnames.length)
                {
                    var cname2 = cnames[index++];
                    cnamelist.addLast(cname2);
                    //
                    if (cname1.equals(cname2)) break;
                }
            }
            else
            {
                cnamelist.addLast(cname1);
            }
        }
        //
        var nextchilds = new ArrayList<PropertySet>(childs.size());
        //
        while (!cnamelist.isEmpty())
        {
            var cname = cnamelist.removeFirst();
            //
            for (var child : childs)
            {
                if (child.hasName(cname)) nextchilds.add(child);
            }
        }
        //
        assert childs.size() == nextchilds.size();
        //
        childs = nextchilds;
    }
    /**
     * Returns the Child Count
     *
     * @return Child Count
     */
    public final int getChildCount()
    {
        if (childs == null) return 0;
        //
        return childs.size();
    }
    /**
     * Returns all Childs
     *
     * @return Childs
     */
    public final PropertySet[] getChilds()
    {
        if (childs == null) return new PropertySet[0];
        //
        var childarray = new PropertySet[childs.size()];
        childs.toArray(childarray);
        //
        return childarray;
    }
    /**
     * Returns the Child with the given Name
     *
     * @param name Name
     * @return Child with given Name
     */
    public final PropertySet getChild(String name)
    {
        if (childs == null) return null;
        //
        for (var child : childs)
        {
            if (child.hasName(name)) return child;
        }
        //
        return null;
    }
    /**
     * Returns the Child at the given Index
     *
     * @param index Index
     * @return Child at given Index
     */
    public final PropertySet getChild(int index)
    {
        return childs.get(index);
    }
}