package propertyset;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/**
 * Created by andreasm
 * Date: 12.05.14
 */
final class Attribute
{
    private String namespace;
    private String name;
    private String value;
    //
    Attribute()
    {
        super();
    }
    //
    public String getNamespace()
    {
        return namespace;
    }
    //
    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }
    //
    public String getName()
    {
        return name;
    }
    //
    public boolean hasName(String name)
    {
        return name.equals(this.name);
    }
    //
    public void setName(String name)
    {
        this.name = name;
    }
    //
    public String getValue()
    {
        return value;
    }
    //
    public void setValue(String value)
    {
        this.value = value;
    }
}