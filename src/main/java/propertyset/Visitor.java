/*
 * Copyright (C) 2003 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package propertyset;
/**
 * @author andreasm
 */
public interface Visitor
{
	int LEAF = 0;
	int DEPTH_FIRST = 1;
	int BREADTH_FIRST = 2;
    int END = 3;
	/**
	 * Method enter
	 * @param propertyset the visitor enters
	 * @return int
	 * @throws VisitorException if visiting is not successful
	 */
	int enter(PropertySet propertyset) throws VisitorException;
	/**
	 * Method leave
	 * @param propertyset the visitor leaves
	 * @throws VisitorException if visiting is not successful
	 */
	void leave(PropertySet propertyset) throws VisitorException;
}
