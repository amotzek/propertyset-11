/*
 * Copyright (C) 2003 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package propertyset;
/**
 * @author andreasm
 */
public class VisitorException extends Exception
{
	public VisitorException()
	{
		super();
	}
	//
	public VisitorException(String arg0)
	{
		super(arg0);
	}
	//
	public VisitorException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}
	//
	public VisitorException(Throwable arg0)
	{
		super(arg0);
	}
}
