/*
 * Copyright (C) 2003 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 29.04.2003
 */
package propertyset;
/**
 * @author andreasm
 */
abstract class Move
{
	final PropertySet propertyset;
	//
	Move(PropertySet propertyset)
	{
		super();
		//
		this.propertyset = propertyset;
	}
	//
	abstract boolean isEnter();
}
