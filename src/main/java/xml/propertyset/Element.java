/*
 * Copyright (C) 2003, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package xml.propertyset;
/*
 * Created on 28.07.2003
 */
import java.util.StringTokenizer;
/**
 * @author Andreas Motzek
 */
abstract class Element
{
	public abstract boolean isTag();
    //
    public abstract boolean isText();
    //
    static String unescape(String str)
    {
        var tokenizer = new StringTokenizer(str, "&;", true);
        var builder = new StringBuilder();
        boolean outside = true;
        //
        while (tokenizer.hasMoreTokens())
        {
            var token = tokenizer.nextToken();
            //
            if (outside)
            {
                if ("&".equals(token))
                {
                    outside = false;
                }
                else
                {
                    builder.append(token);
                }
            }
            else
            {
                if ("amp".equalsIgnoreCase(token))
                {
                    builder.append("&");
                }
                else if ("lt".equalsIgnoreCase(token))
                {
                    builder.append("<");
                }
                else if ("gt".equalsIgnoreCase(token))
                {
                    builder.append(">");
                }
                else if ("quot".equalsIgnoreCase(token))
                {
                    builder.append("\"");
                }
                else if ("apos".equalsIgnoreCase(token))
                {
                    builder.append("'");
                }
                else if (token.startsWith("#"))
                {
                    try
                    {
                        builder.append((char) Integer.parseInt(token.substring(1)));
                    }
                    catch (NumberFormatException e)
                    {
                        throw new IllegalArgumentException("expected a valid entity (" + token + ")");

                    }
                }
                else
                {
                    throw new IllegalArgumentException("expected amp, lt or gt (" + token + ")");
                }
                //
                if (!tokenizer.hasMoreElements()) throw new IllegalArgumentException("expected ; (end of string)");
                //
                token = tokenizer.nextToken();
                //
                if (!token.equals(";")) throw new IllegalArgumentException("expected ; (" + token + ")");
                //
                outside = true;
            }
        }
        //
        return builder.toString();
    }
}
