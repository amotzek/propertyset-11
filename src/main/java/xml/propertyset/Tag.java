/*
 * Copyright (C) 2003, 2004, 2006, 2007, 2010, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 28.07.2003
 */
package xml.propertyset;
//
import java.util.HashMap;
import java.util.Map;
/**
 * @author Andreasm
 */
final class Tag extends Element
{
    private static final int OPENING = 0;
    private static final int CLOSING = 1;
    private static final int EMPTY = 2;
    //
    private String name;
    private final int which;
    private final HashMap<String, String> attributes;
    /**
     * Constructor for Tag
     * 
     * @param tag Tag Name
     */
    @SuppressWarnings("StatementWithEmptyBody")
    Tag(String tag)
    {
        super();
        //
        attributes = new HashMap<>();
        //
        int start = 0;
        int end = tag.length() - 1;
        //
        while (tag.charAt(start) <= ' ')
        {
            start++;
        }
        //
        while (tag.charAt(end) <= ' ')
        {
            end--;
        }
        //
        if (tag.charAt(start) == '/')
        {
            which = CLOSING;
            start++;
        }
        else if (tag.charAt(end) == '/')
        {
            which = EMPTY;
            end--;
            //
            while (tag.charAt(end) == ' ')
            {
                end--;
            }
        }
        else
        {
            which = OPENING;
        }
        //
        int state = 0;
        StringBuilder name = null;
        StringBuilder value = null;
        //
        for (int pos = start; pos <= end; pos++)
        {
            char actual = tag.charAt(pos);
            //
            switch (state)
            {
                case 0:
                    if (actual <= ' ')
                    {
                        // do nothing
                    }
                    else if (actual == '"')
                    {
                        name = new StringBuilder();
                        state = 1;
                    }
                    else
                    {
                        name = new StringBuilder();
                        name.append(actual);
                        state = 2;
                    }
                    break;
                //
                case 1:
                    if (actual == '"')
                    {
                        this.name = name.toString();
                        name = null;
                        state = 3;
                    }
                    else
                    {
                        name.append(actual);
                    }
                    break;
                //
                case 2:
                    if (actual <= ' ')
                    {
                        this.name = name.toString();
                        name = null;
                        state = 3;
                    }
                    else
                    {
                        name.append(actual);
                    }
                    break;
                //
                case 3:
                case 10:
                    if (actual == '"')
                    {
                        name = new StringBuilder();
                        state = 4;
                    }
                    else if (actual > ' ')
                    {
                        name = new StringBuilder();
                        name.append(actual);
                        state = 5;
                    }
                    break;
                //
                case 4:
                    if (actual == '"')
                    {
                        state = 6;
                    }
                    else
                    {
                        name.append(actual);
                    }
                    break;
                //
                case 5:
                    if (actual <= ' ')
                    {
                        state = 6;
                    }
                    else if (actual == '=')
                    {
                        state = 7;
                    }
                    else
                    {
                        name.append(actual);
                    }
                    break;
                //
                case 6:
                    if (actual == '=')
                    {
                        state = 7;
                    }
                    else if (actual == '"')
                    {
                        addAttribute(name.toString());
                        name = new StringBuilder();
                        state = 4;
                    }
                    else if (actual > ' ')
                    {
                        addAttribute(name.toString());
                        name = new StringBuilder();
                        name.append(actual);
                        state = 5;
                    }
                    break;
                //
                case 7:
                    if (actual == '"')
                    {
                        value = new StringBuilder();
                        state = 8;
                    }
                    else if (actual > ' ')
                    {
                        value = new StringBuilder();
                        value.append(actual);
                        state = 9;
                    }
                    break;
                //
                case 8:
                    if (actual == '"')
                    {
                        addAttribute(name.toString(), unescape(value.toString()));
                        name = null;
                        value = null;
                        state = 10;
                    }
                    else
                    {
                        value.append(actual);
                    }
                    break;
                //
                case 9:
                    if (actual <= ' ')
                    {
                        addAttribute(name.toString(), unescape(value.toString()));
                        name = null;
                        value = null;
                        state = 10;
                    }
                    else
                    {
                        value.append(actual);
                    }
                    break;
                //
            }
        }
        //
        switch (state)
        {
            case 2:
                this.name = name.toString();
                break;
            //
            case 5:
            case 6:
                addAttribute(name.toString());
                break;
            //
            case 7:
                addAttribute(name.toString(), "");
                break;
        }
    }
    //
    private void addAttribute(String name)
    {
        attributes.put(name, null);
    }
    //
    private void addAttribute(String name, String value)
    {
        attributes.put(name, value);
    }
    //
    @Override
    public boolean isTag()
    {
        return true;
    }
    //
    @Override
    public boolean isText()
    {
        return false;
    }
    /**
     * Checks if this is an opening Tag
     * 
     * @return true if this is an opening Tag
     */
    boolean isOpening()
    {
        return which == OPENING;
    }
    /**
     * Checks if this is a closing Tag
     * 
     * @return true, if this is a closing Tag
     */
    boolean isClosing()
    {
        return which == CLOSING;
    }
    /**
     * Checks if this Tag is empty
     * 
     * @return true, if this is an empty Tag
     */
    boolean isEmpty()
    {
        return which == EMPTY;
    }
    /**
     * Gets the Tag Name
     * 
     * @return Name
     */
    String getName()
    {
        return name;
    }
    /**
     * Gets the Attributes
     * 
     * @return Attributes
     */
     Map<String, String> getAttributes()
    {
        return attributes;
    }
}