/*
 * Copyright (C) 2003, 2007, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package xml.propertyset;
/*
 * Created on 28.07.2003
 */
import propertyset.Attributes;
import propertyset.PropertySet;
import java.util.HashMap;
/**
 * @author Andreasm
 */
final class State
{
    private final State parent;
    private final PropertySet propertyset;
    private String defaultnamespace;
    private HashMap<String, String> namespaces;
    //
    State(State parent)
	{
	    super();
        //
        this.parent = parent;
        //
        propertyset = new PropertySet();
    }
    //
    void setTag(Tag tag)
    {
        setDefaultNamespace(tag);
        setNamespaces(tag);
        setNameAndNamespace(tag);
        setAttributes(tag);
    }
    //
    private void setDefaultNamespace(Tag tag)
    {
        var attributes = tag.getAttributes();
        //
        if (attributes.containsKey("xmlns"))
        {
            defaultnamespace = attributes.get("xmlns");
        }
        else if (parent != null)
        {
            defaultnamespace = parent.defaultnamespace;
        }
    }
    //
    private void setNamespaces(Tag tag)
    {
        namespaces = new HashMap<>();
        var map = tag.getAttributes();
        var iterator = map.entrySet().iterator();
        //
        while (iterator.hasNext())
        {
            var entry = iterator.next();
            var attributename = entry.getKey();
            var attributevalue = entry.getValue();
            //
            if (attributename.startsWith("xmlns:"))
            {
                var namespacename = attributename.substring(6);
                namespaces.put(namespacename, attributevalue);
                iterator.remove();
            }
        }
    }
    //
    private void setNameAndNamespace(Tag tag)
    {
        var namespace = getNamespace(tag);
        var name = getName(tag);
        propertyset.setNamespace(namespace);
        propertyset.setName(name);
    }
    //
    private String getNamespace(Tag tag)
    {
        var tagname = tag.getName();
        int colonindex = tagname.indexOf(':');
        var namespace = defaultnamespace;
        //
        if (colonindex >= 0)
        {
            var namespacename = tagname.substring(0, colonindex);
            namespace = findNamespace(this, namespacename);
        }
        //
        return namespace;
    }
    //
    private static String getName(Tag tag)
    {
        var tagname = tag.getName();
        int colonindex = tagname.indexOf(':');
        //
        if (colonindex < 0) return tagname;
        //
        return tagname.substring(colonindex + 1);
    }
    //
    private static String findNamespace(State state, String namespacename)
    {
        while (state != null)
        {
            var namespace = state.namespaces.get(namespacename);
            //
            if (namespace != null) return namespace;
            //
            state = state.parent;
        }
        //
        return null;
    }
    //
    private void setAttributes(Tag tag)
    {
        var attributemap = tag.getAttributes();
        //
        if (attributemap.isEmpty()) return;
        //
        var attributes = new Attributes();
        //
        for (var entry : attributemap.entrySet())
        {
            var attributename = entry.getKey();
            var attributevalue = entry.getValue();
            int colonindex = attributename.indexOf(':');
            var namespace = defaultnamespace;
            var name = attributename;
            //
            if (colonindex >= 0)
            {
                var namespacename = attributename.substring(0, colonindex);
                namespace = findNamespace(this, namespacename);
                //
                if (namespace != null) name = attributename.substring(colonindex + 1);
            }
            //
            attributes.setAttribute(namespace, name, attributevalue);
        }
        //
        propertyset.setAttributes(attributes);
    }
    //
    State getParent()
    {
        return parent;
    }
	//
    boolean matches(Tag tag)
	{
        var namespace = getNamespace(tag);
        var name = getName(tag);
        //
		return propertyset.hasNamespace(namespace) && propertyset.hasName(name);
	}
	//
    PropertySet getPropertySet()
	{
		return propertyset;
	}
	//
    void addChild(PropertySet child)
	{
		propertyset.addChild(child);
	}
	//
    void setValue(String value)
	{
		propertyset.setValue(value);
	}
}