/*
 * Copyright (C) 2006, 2007 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package xml.propertyset;
//
import propertyset.PropertySet;
/*
 * Created on 19.06.2006
 */
public final class PropertySetFormatter
{
    private final PropertySet propertyset;
    private StringBuilder builder;
    //
    public PropertySetFormatter(PropertySet propertyset)
    {
        super();
        //
        this.propertyset = propertyset;
    }
    //
    public synchronized String getXML()
    {
        if (builder == null)
        {
            builder = new StringBuilder();
            append(propertyset);
        }
        //
        return builder.toString();
    }
    //
    private void append(PropertySet parent)
    {
        var name = parent.getName();
        builder.append("<");
        builder.append(name);
        var attributes = parent.getAttributes();
        //
        if (attributes != null)
        {
            var names = attributes.getNames();
            //
            for (var attributename : names)
            {
                var value = attributes.getAttribute(attributename);
                builder.append(" ");
                builder.append(attributename);
                builder.append("=\"");
                appendEscaped(value);
                builder.append("\"");
            }
        }
        //
        builder.append(">");
        var value = parent.getValue();
        //
        if (value != null) appendEscaped(value);
        //
        int nchilds = parent.getChildCount();
        //
        for (int i = 0; i < nchilds; i++)
        {
            var child = parent.getChild(i);
            append(child);
        }
        //
        builder.append("</");
        builder.append(name);
        builder.append(">");
        builder.append('\n');
    }
    //
    private void appendEscaped(String value)
    {
        for (int i = 0; i < value.length(); i++)
        {
            final char c = value.charAt(i);
            //
            switch (c)
            {
                case '&':
                    builder.append("&amp;");
                    break;
                //
                case '<':
                    builder.append("&lt;");
                    break;
                //
                case '>':
                    builder.append("&gt;");
                    break;
                //
                case '"':
                    builder.append("&quot;");
                    break;
                //
                case '\'':
                    builder.append("&apos;");
                    break;
                //
                default:
                    builder.append(c);
            }
        }
    }
}
