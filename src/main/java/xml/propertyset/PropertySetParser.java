package xml.propertyset;
/*
 * Copyright (C) 2003 - 2005, 2007, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import propertyset.PropertySet;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.NoSuchElementException;
/**
 * @author andreasm
 */
public class PropertySetParser
{
    private final Reader reader;
    private final PropertySet root;
    private State state;
    private int next;
    /**
     * Constructor for PropertySetParser
     * 
     * @param reader Reader
     * @throws java.io.IOException if the Contents of the Reader cannot be read
     */
    public PropertySetParser(Reader reader) throws IOException
    {
        super();
        //
        this.reader = reader;
        //
        next = reader.read();
        root = parse();
    }
    /**
     * Constructor for PropertySetParser
     *
     * @param xml String
     * @throws java.io.IOException if the Contents are not welformed
     */
    public PropertySetParser(String xml) throws IOException
    {
        this(new StringReader(xml));
    }
    /**
     * Returns the Property Set
     * 
     * @return Property Set
     */
    public PropertySet getPropertySet()
    {
        return root;
    }
    //
    private PropertySet parse() throws IOException
    {
        var element = nextElement();
        //
        while (!element.isTag())
        {
            element = nextElement();
        }
        //
        var tag = (Tag) element;
        //
        if (tag.isClosing()) throw new IOException("unexpected closing tag " + tag.getName());
        //
        if (tag.isOpening())
        {
            state = new State(state);
            state.setTag(tag);
        }
        else
        {
            return fromEmpty(tag);
        }
        //
        try
        {
            while (state != null)
            {
                element = nextElement();
                //
                if (element.isTag())
                {
                    tag = (Tag) element;
                    //
                    if (tag.isClosing())
                    {
                        if (!state.matches(tag)) throw new IOException("unexpected closing tag " + tag.getName());
                        //
                        var parentstate = state.getParent();
                        //
                        if (parentstate == null) return state.getPropertySet();
                        //
                        parentstate.addChild(state.getPropertySet());
                        state = parentstate;
                    }
                    else if (tag.isOpening())
                    {
                        state = new State(state);
                        state.setTag(tag);
                    }
                    else
                    {
                        state.addChild(fromEmpty(tag));
                    }
                }
                else if (element.isText())
                {
                    Text text = (Text) element;
                    state.setValue(text.getText());
                }
            }
        }
        catch (NoSuchElementException ignore)
        {
        }
        //
        return null;
    }
    //
    private PropertySet fromEmpty(Tag tag)
    {
        assert tag.isEmpty();
        //
        var leafstate = new State(state);
        leafstate.setTag(tag);
        //
        return leafstate.getPropertySet();
    }
    //
    private void readNext() throws IOException
    {
        next = reader.read();
        //
        if (next < 0) throw new EOFException();
    }
    //
    private Element nextElement() throws IOException
    {
        while (next <= ' ')
        {
            readNext();
        }
        //
        if (next == '<')
        {
            readNext();
            //
            StringBuilder builder = new StringBuilder();
            //
            while (next != '>')
            {
                builder.append((char) next);
                readNext();
            }
            //
            if (startsWith(builder, "?"))
            {
                while (!endsWith(builder, "?"))
                {
                    builder.append((char) next);
                    readNext();
                    //
                    while (next != '>')
                    {
                        builder.append((char) next);
                        readNext();
                    }
                }
                //
                assert next == '>';
                //
                readNext();
                //
                return Comment.getInstance();
            }
            //
            if (startsWith(builder, "!--"))
            {
                while (!endsWith(builder, "--"))
                {
                    builder.append((char) next);
                    readNext();
                    //
                    while (next != '>')
                    {
                        builder.append((char) next);
                        readNext();
                    }
                }
                //
                readNext();
                //
                return Comment.getInstance();
            }
            //
            try
            {
                readNext();
            }
            catch (EOFException ignore)
            {
            }
            //
            return new Tag(builder.toString());
        }
        //
        var builder = new StringBuilder();
        //
        while (next != '<')
        {
            builder.append((char) next);
            readNext();
        }
        //
        return new Text(builder.toString().trim());
    }
    //
    private static boolean startsWith(StringBuilder builder, String prefix)
    {
        int length = prefix.length();
        //
        if (builder.length() < length) return false;
        //
        return builder.substring(0, length).equals(prefix);
    }
    //
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private static boolean endsWith(StringBuilder builder, String postfix)
    {
        int length = postfix.length();
        //
        if (builder.length() < length) return false;
        //
        return builder.substring(builder.length() - length).equals(postfix);
    }
}