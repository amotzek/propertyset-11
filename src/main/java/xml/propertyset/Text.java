/*
 * Copyright (C) 2003, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created on 28.07.2003
 */
package xml.propertyset;
/**
 * @author Andreasm
 */
final class Text extends Element
{
    private final String text;
    //
    Text(String text)
    {
        super();
        // 
        this.text = unescape(text);
    }
    //
    @Override
    public boolean isTag()
    {
        return false;
    }
    //
    @Override
    public boolean isText()
    {
        return true;
    }
    /**
     * @return Text
     */
    String getText()
    {
        return text;
    }
    //
    public String toString()
    {
        return text;
    }
}
